# Microscopy Data Management for imaging facility at IGBMC

## Instructions

Please make a copy of the required files for your own experiments and use them as template.

## Lightsheet Sample mounting and imaging orientation

For each image acquisition fill the _**LS7_sample_orientation.pdf**_ form to keep track of the sample mounting and imaging orientation.

## Metadata

### Zeiss LightSheet 7

The files:

- LS_img.txt
- LS_sample.txt

Contain guidelines on imaging and sample metadata that are very important to save and keep track of. When you acquire new data or plan a new experiment on the LS7, please make sure to use these templates for the metadata before and during the acquisition.

#### Rules for file naming

- The **LS_sample.txt** is the sample metada companion file. We are using the following rules to name the file:
_**sampleID_dateAcquisition_strain_stage_tissue_metadata.txt**_

sampleID = [unique identifier given to the sample by the LabGuru Samples LightSheet Inventory]

dateAcquisition = [date of the image acquisition in YearMonthFormat]

strain = [cell line, genoype,  etc]

stage = [age or stage of development]

tissue = [tissue description of the sample i.e. brain, kidney]


It should look like **_SL-23-0001_20230210_Sox2-GFP_E18_brain_metadata.txt_**

- The **LS_img.txt** is the image metada companion file. Each image must have a metadata companion file. We are using the following rules to name the file: **sampleID__dateAcquisition_strain_stage_tissue_stack_mosaic_labels_imgID.txt_**. The same rules apply to both the image file .czi and .txt medata file.

sampleID = [unique identifier given to the sample by the LabGuru Samples LightSheet Inventory]

dateAcquisition = [date of the image acquisition in YearMonthDay format]
strain = [cell line, genoype,  etc]

stage = [age or stage of development]

tissue = [tissue description of the sample i.e. brain, kidney]

stack = [add if the image is stack]

mosaic = [add if the image is mosaic]

labels = [list the label(s) acquired]

imgID = [the image ID, starts at 001 and use +1 increment, every image must have a unique imgID]

It should look like **_SL-23-0001_20230210_Sox2-GFP_E18_brain_stack_mosaic_DAPI_GFP_img005.txt_**
